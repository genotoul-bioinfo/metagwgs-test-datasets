Assembly                    sample1     sample2   
# contigs (>= 0 bp)         4           6         
# contigs (>= 1000 bp)      4           6         
# contigs (>= 5000 bp)      3           6         
# contigs (>= 10000 bp)     3           5         
# contigs (>= 25000 bp)     1           3         
# contigs (>= 50000 bp)     0           1         
Total length (>= 0 bp)      76976       171748    
Total length (>= 1000 bp)   76976       171748    
Total length (>= 5000 bp)   72297       171748    
Total length (>= 10000 bp)  72297       164859    
Total length (>= 25000 bp)  40500       133285    
Total length (>= 50000 bp)  0           55211     
# contigs                   4           6         
Largest contig              40500       55211     
Total length                76976       171748    
GC (%)                      39.02       40.99     
N50                         40500       49362     
N90                         13170       13409     
auN                         28353.7     39979.9   
L50                         1           2         
L90                         3           5         
# N's per 100 kbp           0.00        0.00      
# predicted rRNA genes      0 + 0 part  0 + 0 part
