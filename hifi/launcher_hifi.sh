#!/bin/bash
#SBATCH -J mtd
#SBATCH --mem=2GB

METAGWGS="/path/to/metagwgs"

# set variables 
DATA="/path/to/metagwgs-test-datasets/hifi"

# metaflye 
mkdir output
cd output 
sbatch --mem=2GB -J mtd_hifi_metaflye --output=slurm-%x.%j.out  --wrap="
module load devel/java/17.0.6
module load bioinfo/Nextflow/23.10.0
module load containers/singularity/3.9.9
module load devel/python/Python-3.11.1
nextflow run -profile functional_test $METAGWGS/main.nf  --type \"HiFi\" \
														 --input \"$DATA/input/samplesheet.csv\" \
														 --assembly \"metaflye\" \
														 -with-report -with-timeline -with-trace -with-dag -resume

 python $METAGWGS/functional_tests/main.py -step 08_binning -exp_dir $DATA/output -obs_dir ./results > func_test_metaflye.out
 "
cd ..

# assembly 
mkdir assembly
cd assembly 
sbatch --mem=2GB -J mtd_hifi_assembly --output=slurm-%x.%j.out  --wrap="
module load devel/java/17.0.6
module load bioinfo/Nextflow/23.10.0
module load containers/singularity/3.9.9
module load devel/python/Python-3.11.1
nextflow run -profile functional_test $METAGWGS/main.nf  --type \"HiFi\" \
														 --input \"$DATA/input/samplesheet_assembly.csv\" \
														 --assembly \"metaflye\" \
														 -with-report -with-timeline -with-trace -with-dag -resume

python $METAGWGS/functional_tests/main.py -step 08_binning -exp_dir $DATA/output -obs_dir ./results > func_test_assembly.out
 "
cd ..

# hifiasm 
mkdir hifiasm
cd hifiasm 
sbatch --mem=2GB -J mtd_hifi_hifiasm --output=slurm-%x.%j.out  --wrap="
module load devel/java/17.0.6
module load bioinfo/Nextflow/23.10.0
module load containers/singularity/3.9.9
module load devel/python/Python-3.11.1
nextflow run -profile functional_test $METAGWGS/main.nf  --type \"HiFi\" \
														 --input \"$DATA/input/samplesheet.csv\" \
														-with-report -with-timeline -with-trace -with-dag -resume

python $METAGWGS/functional_tests/main.py -step 08_binning -exp_dir $DATA/output_hifiasm -obs_dir ./results > func_test_hifiasm.out
"
cd ..


# flowcell
mkdir output_flowcell
cd output_flowcell
sbatch --mem=2GB -J mtd_hifi_flowcell --output=slurm-%x.%j.out  --wrap="
module load devel/java/17.0.6
module load bioinfo/Nextflow/23.10.0
module load containers/singularity/3.9.9
module load devel/python/Python-3.11.1
nextflow run -profile functional_test $METAGWGS/main.nf --type \"HiFi\" \
														--input \"$DATA/input/samplesheet_flowcell.csv\" \
														--assembly \"metaflye\" \
														-with-report -with-timeline -with-trace -with-dag -resume

python $METAGWGS/functional_tests/main.py -step 08_binning -exp_dir $DATA/output_flowcell -obs_dir ./results > func_test_flowcell.out
" 
cd ..

# group_binning
mkdir output_group_binning
cd output_group_binning
sbatch --mem=2GB -J mtd_hifi_group_binning --output=slurm-%x.%j.out  --wrap="
module load devel/java/17.0.6
module load bioinfo/Nextflow/23.10.0
module load containers/singularity/3.9.9
module load devel/python/Python-3.11.1
nextflow run -profile functional_test $METAGWGS/main.nf --type \"HiFi\" \
														--input \"$DATA/input/samplesheet_group_binning.csv\" \
														--assembly \"metaflye\" \
														--binning_cross_alignment \"group\" \
														-with-report -with-timeline -with-trace -with-dag -resume

 python $METAGWGS/functional_tests/main.py -step 08_binning -exp_dir $DATA/output_group_binning -obs_dir ./results > func_test_group_binning.out
" 
cd ..

# all_binning
mkdir output_all_binning
cd output_all_binning
sbatch --mem=2GB -J mtd_hifi_all_binning --output=slurm-%x.%j.out  --wrap="
module load devel/java/17.0.6
module load bioinfo/Nextflow/23.10.0
module load containers/singularity/3.9.9
module load devel/python/Python-3.11.1
nextflow run -profile functional_test $METAGWGS/main.nf --type \"HiFi\" \
														--input \"$DATA/input/samplesheet_assembly.csv\" \
														--assembly \"metaflye\" \
														--binning_cross_alignment \"all\" \
														-with-report -with-timeline -with-trace -with-dag -resume

 python $METAGWGS/functional_tests/main.py -step 08_binning -exp_dir $DATA/output_all_binning -obs_dir ./results > func_test_all_binning.out
" 
cd ..


# coassembly
mkdir output_coassembly
cd output_coassembly
sbatch --mem=2GB -J mtd_hifi_coassembly --output=slurm-%x.%j.out  --wrap="
module load devel/java/17.0.6
module load bioinfo/Nextflow/23.10.0
module load containers/singularity/3.9.9
module load devel/python/Python-3.11.1
nextflow run -profile functional_test $METAGWGS/main.nf --type \"HiFi\" \
														--input \"$DATA/input/samplesheet_coassembly.csv\" \
														--assembly \"metaflye\" \
                                                        --coassembly \
														-with-report -with-timeline -with-trace -with-dag -resume 
 python $METAGWGS/functional_tests/main.py -step 08_binning -exp_dir $DATA/output_coassembly -obs_dir ./results > func_test_coassembly.out
" 
cd ..
