Assembly                    1           2         
# contigs (>= 0 bp)         9           6         
# contigs (>= 1000 bp)      9           6         
# contigs (>= 5000 bp)      9           6         
# contigs (>= 10000 bp)     5           5         
# contigs (>= 25000 bp)     3           3         
# contigs (>= 50000 bp)     2           1         
Total length (>= 0 bp)      232313      171748    
Total length (>= 1000 bp)   232313      171748    
Total length (>= 5000 bp)   232313      171748    
Total length (>= 10000 bp)  203049      164859    
Total length (>= 25000 bp)  166089      133285    
Total length (>= 50000 bp)  125592      55211     
# contigs                   9           6         
Largest contig              75228       55211     
Total length                232313      171748    
GC (%)                      40.96       40.99     
N50                         50364       49362     
N90                         9358        13409     
auN                         46466.0     39979.9   
L50                         2           2         
L90                         6           5         
# N's per 100 kbp           0.00        0.00      
# predicted rRNA genes      0 + 1 part  0 + 0 part
