# Test datasets repository
## Hifi data

The reads have been subsampled from mock metagenome MSA 1003 sequenced in HiFi (https://www.nature.com/articles/s41597-020-00743-4).

### Input
3 samples (reads):
* sample1
* sample2.1
* sample2.2: clone of sample2.1


### Script 
The script launcher_hifi.sh was used to obtain the output results. It launches metagwgs in different conditions (on the genologin server) with the inputs of 3genomes and compares the obtained results with the expected results.