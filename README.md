# Test datasets repository
```
git clone https://forgemia.inra.fr/genotoul-bioinfo/metagwgs-test-datasets.git
or git clone git@forgemia.inra.fr:genotoul-bioinfo/metagwgs-test-datasets.git
```
# Test databanks repository
Get data banks: download this archive and decompress its contents in any folder.
```
wget http://genoweb.toulouse.inra.fr/~choede/FT_banks.tgz
tar -xvzf FT_banks.tgz
```
## Small data
### Input
host: Homo_Sapiens.GRCh38 chromosome 21 (full) + BWA index

Illumina samples: a1, a2 (clone of a1) and c - paired reads

### Output
8 steps of metagWGS (S01 to S08): expected output files for functional tests

## MAG data
### Input
host: Homo_Sapiens.GRCh38 chromosome 21 (full) + BWA index

Illumina samples: test_minigut_sample1, test_minigut_sample2.1 and test_minigut_sample2.2 (clone of test_minigut_sample2) - paired reads

### Output
8 steps of metagWGS (S01 to S08): expected output files for functional tests

## Hifi data
### Input
Pacbio Hifi samples: sample1 and sample2 - assemblies and reads

### Output
8 steps of metagWGS (S01 to S08): expected output files for functional tests

## 3genomes data
### Input
Illumina samples: 3genomes_1.1, 3genomes_1.2 (clone of 3genomes_1.1) and 3genomes_2 - assemblies and paired reads 

### Output
8 steps of metagWGS (S01 to S08): expected output files for functional tests
