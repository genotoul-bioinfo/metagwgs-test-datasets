#!/bin/bash
#SBATCH -J mtd
#SBATCH --mem=2GB

METAGWGS="/path/to/metagwgs"

# set variables 
DATA="/path/to/metagwgs-test-datasets/small"

# create directory

# metaspades 
mkdir output
cd output 
sbatch --mem=2GB -J mtd_small_metaspades --output=slurm-%x.%j.out  --wrap="
module load devel/java/17.0.6
module load bioinfo/Nextflow/23.10.0
module load containers/singularity/3.9.9
module load devel/python/Python-3.11.1
nextflow run -profile functional_test $METAGWGS/main.nf  --type \"SR\" \
														 --input \"$DATA/input/samplesheet.csv\" \
														 --min_contigs_cpm 1000 \
														 -with-report -with-timeline -with-trace -with-dag -resume

python $METAGWGS/functional_tests/main.py -step 08_binning -exp_dir $DATA/output -obs_dir ./results > func_test_metaspades.out
"
cd ..

#megahit
mkdir output_megahit
cd output_megahit
sbatch --mem=2GB -J mtd_small_megahit --output=slurm-%x.%j.out  --wrap="
module load devel/java/17.0.6
module load bioinfo/Nextflow/23.10.0
module load containers/singularity/3.9.9
module load devel/python/Python-3.11.1
nextflow run -profile functional_test $METAGWGS/main.nf --type \"SR\" \
														--input \"$DATA/input/samplesheet.csv\" \
														--min_contigs_cpm 1000 \
														--assembly \"megahit\" \
														-with-report -with-timeline -with-trace -with-dag -resume 

python $METAGWGS/functional_tests/main.py -step 08_binning -exp_dir $DATA/output_megahit -obs_dir ./results > func_test_megahit.out
"
cd ..

# assembly
mkdir assembly
cd assembly
sbatch --mem=2GB -J mtd_small_assembly --output=slurm-%x.%j.out  --wrap="
module load devel/java/17.0.6
module load bioinfo/Nextflow/23.10.0
module load containers/singularity/3.9.9
module load devel/python/Python-3.11.1
nextflow run -profile functional_test $METAGWGS/main.nf --type \"SR\" \
														--input \"$DATA/input/samplesheet_assembly.csv\" \
														--min_contigs_cpm 1000 \
														-with-report -with-timeline -with-trace -with-dag -resume

python $METAGWGS/functional_tests/main.py -step 08_binning -exp_dir $DATA/output -obs_dir ./results > func_test_assembly.out
"
cd ..

# flowcell
mkdir output_flowcell
cd output_flowcell
sbatch --mem=2GB -J mtd_small_flowcell --output=slurm-%x.%j.out  --wrap="
module load devel/java/17.0.6
module load bioinfo/Nextflow/23.10.0
module load containers/singularity/3.9.9
module load devel/python/Python-3.11.1
nextflow run -profile functional_test $METAGWGS/main.nf --type \"SR\" \
														--input \"$DATA/input/samplesheet_flowcell.csv\" \
														--min_contigs_cpm 1000 \
														-with-report -with-timeline -with-trace -with-dag -resume

python $METAGWGS/functional_tests/main.py -step 08_binning -exp_dir $DATA/output_flowcell -obs_dir ./results > func_test_flowcell.out
" 
cd .. 

# small 
mkdir output_coassembly
cd output_coassembly 
sbatch --mem=2GB -J mtd_small_metaspades --output=slurm-%x.%j.out  --wrap="
module load devel/java/17.0.6
module load bioinfo/Nextflow/23.10.0
module load containers/singularity/3.9.9
module load devel/python/Python-3.11.1
nextflow run -profile functional_test $METAGWGS/main.nf  --type \"SR\" \
														 --input \"$DATA/input/samplesheet_coassembly.csv\" \
														 --min_contigs_cpm 1000 \
														 --coassembly \
														 -with-report -with-timeline -with-trace -with-dag -resume

python $METAGWGS/functional_tests/main.py -step 08_binning -exp_dir $DATA/output_coassembly -obs_dir ./results > func_test_metaspades.out
" 
cd ..
