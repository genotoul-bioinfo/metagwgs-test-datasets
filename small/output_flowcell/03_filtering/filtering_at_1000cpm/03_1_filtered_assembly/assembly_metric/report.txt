Assembly                    a_select_contigs_cpm1000  c_select_contigs_cpm1000
# contigs (>= 0 bp)         406                       41                      
# contigs (>= 1000 bp)      0                         18                      
# contigs (>= 5000 bp)      0                         7                       
# contigs (>= 10000 bp)     0                         4                       
# contigs (>= 25000 bp)     0                         0                       
# contigs (>= 50000 bp)     0                         0                       
Total length (>= 0 bp)      99942                     121666                  
Total length (>= 1000 bp)   0                         106386                  
Total length (>= 5000 bp)   0                         84536                   
Total length (>= 10000 bp)  0                         61319                   
Total length (>= 25000 bp)  0                         0                       
Total length (>= 50000 bp)  0                         0                       
# contigs                   406                       41                      
Largest contig              457                       17666                   
Total length                99942                     121666                  
GC (%)                      45.33                     32.06                   
N50                         244                       13447                   
N90                         213                       831                     
auN                         251.5                     9867.7                  
L50                         183                       4                       
L90                         358                       22                      
# N's per 100 kbp           0.00                      267.12                  
# predicted rRNA genes      0 + 0 part                0 + 0 part              
