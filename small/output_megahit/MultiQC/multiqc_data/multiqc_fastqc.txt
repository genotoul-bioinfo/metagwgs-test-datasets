Sample	Filename	File type	Encoding	Total Sequences	Sequences flagged as poor quality	Sequence length	%GC	total_deduplicated_percentage	avg_sequence_length	basic_statistics	per_base_sequence_quality	per_tile_sequence_quality	per_sequence_quality_scores	per_base_sequence_content	per_sequence_gc_content	per_base_n_content	sequence_length_distribution	sequence_duplication_levels	overrepresented_sequences	adapter_content
a1_R1	a1_R1.fastq.gz	Conventional base calls	Sanger / Illumina 1.9	25000.0	0.0	150.0	46.0	93.57600000000001	150.0	pass	fail	pass	pass	pass	pass	pass	pass	pass	pass	pass
a1_R2	a1_R2.fastq.gz	Conventional base calls	Sanger / Illumina 1.9	25000.0	0.0	150.0	47.0	99.212	150.0	pass	fail	pass	pass	pass	pass	pass	pass	pass	pass	pass
a2_R1	a2_R1.fastq.gz	Conventional base calls	Sanger / Illumina 1.9	25000.0	0.0	150.0	46.0	93.57600000000001	150.0	pass	fail	pass	pass	pass	pass	pass	pass	pass	pass	pass
a2_R2	a2_R2.fastq.gz	Conventional base calls	Sanger / Illumina 1.9	25000.0	0.0	150.0	47.0	99.212	150.0	pass	fail	pass	pass	pass	pass	pass	pass	pass	pass	pass
c_R1	c_R1.fastq.gz	Conventional base calls	Sanger / Illumina 1.9	25000.0	0.0	150.0	40.0	92.97999999999999	150.0	pass	fail	pass	pass	pass	warn	pass	pass	pass	pass	pass
c_R2	c_R2.fastq.gz	Conventional base calls	Sanger / Illumina 1.9	25000.0	0.0	150.0	41.0	98.78	150.0	pass	fail	pass	pass	pass	pass	pass	pass	pass	pass	pass
