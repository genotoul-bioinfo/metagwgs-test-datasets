# Test datasets repository
## Small data
### Input
3 samples (paired reads):
* a1: small sample 1
* c: small sample 2
* a2: clone of a1

Host Human GRCh38, chromosome 21 (full):
* Homo_sapiens.GRCh38_chr21.fa: fasta
* Homo_sapiens.GRCh38_chr21.fa.\{0123,amb,ann,bwt.2bit.64,pac,sa\}: bwa-mem2 index

### Script 
The script launcher_small.sh was used to obtain the output results. It launches metagwgs in different conditions (on the genologin server) with the inputs of 3genomes and compares the obtained results with the expected results.