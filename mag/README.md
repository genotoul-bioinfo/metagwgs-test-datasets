# Test datasets repository
## Mag data
source: [nf-core\/test-datasets](https://github.com/nf-core/test-datasets/tree/mag/test_data)
paper: [Bertrand et al. Nature Biotechnology (2019)](https://www.nature.com/articles/s41587-019-0191-2)

### Input
3 samples (paired reads):
* test_minigut_sample1: mag minigut sample 1
* test_minigut_sample2.1: mag minigut sample 2
* test_minigut_sample2.2: clone of test_minigut_sample2.1

Host Human GRCh38, chromosome 21 (full):
* Homo_sapiens.GRCh38_chr21.fa: fasta
* Homo_sapiens.GRCh38_chr21.fa.\{0123,amb,ann,bwt.2bit.64,pac,sa\}: bwa-mem2 index
