Module	Section	Sample Name	Source
FastQC (raw)	all_sections	3_genomes_1.2_R2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/fastqc_raw_report/3_genomes_1.2_R2_fastqc.zip
FastQC (raw)	all_sections	3_genomes_2_R2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/fastqc_raw_report/3_genomes_2_R2_fastqc.zip
FastQC (raw)	all_sections	3_genomes_1.1_R1	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/fastqc_raw_report/3_genomes_1.1_R1_fastqc.zip
FastQC (raw)	all_sections	3_genomes_1.1_R2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/fastqc_raw_report/3_genomes_1.1_R2_fastqc.zip
FastQC (raw)	all_sections	3_genomes_2_R1	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/fastqc_raw_report/3_genomes_2_R1_fastqc.zip
FastQC (raw)	all_sections	3_genomes_1.2_R1	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/fastqc_raw_report/3_genomes_1.2_R1_fastqc.zip
Cutadapt	all_sections	3_genomes_2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/cutadapt_report/3_genomes_2_cutadapt.log
Cutadapt	all_sections	3_genomes_1.2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/cutadapt_report/3_genomes_1.2_cutadapt.log
Cutadapt	all_sections	3_genomes_1.1	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/cutadapt_report/3_genomes_1.1_cutadapt.log
Reads mapping the host genome	flagstat	3_genomes_2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/reads_on_host_genome/3_genomes_2.no_filter.flagstat
Reads mapping the host genome	flagstat	3_genomes_1.1	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/reads_on_host_genome/3_genomes_1.1.no_filter.flagstat
Reads mapping the host genome	flagstat	3_genomes_1.2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/reads_on_host_genome/3_genomes_1.2.no_filter.flagstat
FastQC (cleaned)	all_sections	3_genomes_2_R2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/fastqc_clean_report/cleaned_3_genomes_2_R2_fastqc.zip
FastQC (cleaned)	all_sections	3_genomes_1.2_R2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/fastqc_clean_report/cleaned_3_genomes_1.2_R2_fastqc.zip
FastQC (cleaned)	all_sections	3_genomes_2_R1	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/fastqc_clean_report/cleaned_3_genomes_2_R1_fastqc.zip
FastQC (cleaned)	all_sections	3_genomes_1.1_R1	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/fastqc_clean_report/cleaned_3_genomes_1.1_R1_fastqc.zip
FastQC (cleaned)	all_sections	3_genomes_1.2_R1	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/fastqc_clean_report/cleaned_3_genomes_1.2_R1_fastqc.zip
FastQC (cleaned)	all_sections	3_genomes_1.1_R2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/fastqc_clean_report/cleaned_3_genomes_1.1_R2_fastqc.zip
Kaiju	all_sections	3_genomes_2_kaiju_MEM	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/kaiju_report/3_genomes_2_kaiju_MEM.out.summary_class
Kaiju	all_sections	3_genomes_1.2_kaiju_MEM	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/kaiju_report/3_genomes_1.2_kaiju_MEM.out.summary_genus
Kaiju	all_sections	3_genomes_1.1_kaiju_MEM	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/kaiju_report/3_genomes_1.1_kaiju_MEM.out.summary_order
Quast primary assembly	all_sections	3_genomes_1.2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/quast_primary/report.tsv
Quast primary assembly	all_sections	3_genomes_1.1	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/quast_primary/report.tsv
Quast primary assembly	all_sections	3_genomes_2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/quast_primary/report.tsv
Reads alignment on unfiltered assembly	flagstat	3_genomes_1.1	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/unfiltered_assembly_flagstat/3_genomes_1.1.flagstat
Reads alignment on unfiltered assembly	flagstat	3_genomes_2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/unfiltered_assembly_flagstat/3_genomes_2.flagstat
Reads alignment on unfiltered assembly	flagstat	3_genomes_1.2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/unfiltered_assembly_flagstat/3_genomes_1.2.flagstat
Quast filtered assembly	all_sections	3_genomes_1.2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/quast_filtered/report.tsv
Quast filtered assembly	all_sections	3_genomes_2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/quast_filtered/report.tsv
Quast filtered assembly	all_sections	3_genomes_1.1	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/quast_filtered/report.tsv
Reads alignment on final assembly	flagstat	3_genomes_1.1	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/final_assembly_flagstat/3_genomes_1.1.flagstat
Reads alignment on final assembly	flagstat	3_genomes_2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/final_assembly_flagstat/3_genomes_2.flagstat
Reads alignment on final assembly	flagstat	3_genomes_1.2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/final_assembly_flagstat/3_genomes_1.2.flagstat
Structural annotation	all_sections	3_genomes_1.1	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/prokka_report/3_genomes_1.1.txt
Structural annotation	all_sections	3_genomes_1.2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/prokka_report/3_genomes_1.2.txt
Structural annotation	all_sections	3_genomes_2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/prokka_report/3_genomes_2.txt
featureCounts	all_sections	3_genomes_2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/featureCounts_report/3_genomes_2.featureCounts.tsv.summary
featureCounts	all_sections	3_genomes_1.1	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/featureCounts_report/3_genomes_1.1.featureCounts.tsv.summary
featureCounts	all_sections	3_genomes_1.2	/work/mvienne/maj_mtd/3genomes/metaspades/work/a5/7dd9df26e96c6db89bf68ddba14f28/featureCounts_report/3_genomes_1.2.featureCounts.tsv.summary
