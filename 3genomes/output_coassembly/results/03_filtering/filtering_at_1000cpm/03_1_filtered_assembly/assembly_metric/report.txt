Assembly                    1_select_contigs_cpm1000  2_select_contigs_cpm1000
# contigs (>= 0 bp)         17                        21                      
# contigs (>= 1000 bp)      12                        16                      
# contigs (>= 5000 bp)      0                         0                       
# contigs (>= 10000 bp)     0                         0                       
# contigs (>= 25000 bp)     0                         0                       
# contigs (>= 50000 bp)     0                         0                       
Total length (>= 0 bp)      27977                     29783                   
Total length (>= 1000 bp)   24832                     25644                   
Total length (>= 5000 bp)   0                         0                       
Total length (>= 10000 bp)  0                         0                       
Total length (>= 25000 bp)  0                         0                       
Total length (>= 50000 bp)  0                         0                       
# contigs                   17                        21                      
Largest contig              4292                      2583                    
Total length                27977                     29783                   
GC (%)                      40.70                     40.63                   
N50                         2291                      1514                    
N90                         912                       939                     
auN                         2384.9                    1596.9                  
L50                         5                         8                       
L90                         13                        18                      
# N's per 100 kbp           0.00                      0.00                    
# predicted rRNA genes      2 + 1 part                3 + 1 part              
