Sample	genome_name	completeness	contamination	genome_length	genome_N50	contig_count	sum_numreads	sum_meandepth
3_genomes_1.1_bins.0	s__Anaerostipes hadrus	98.28	0.64	2890775.0	62523.0	59.0	46415.0	2.367127476219295
3_genomes_2_bins.0	s__Lactobacillus helveticus	99.87	2.79	1765872.0	11807.0	203.0	70581.0	5.817417902695096
3_genomes_2_bins.1	s__Clostridium_W aceticum	81.08	3.57	3702376.0	3633.0	1180.0	22661.0	0.895992455052592
3_genomes_2_bins.2	s__Bifidobacterium breve	99.99	0.18	2351912.0	169624.0	26.0	75572.0	4.737000035707968
