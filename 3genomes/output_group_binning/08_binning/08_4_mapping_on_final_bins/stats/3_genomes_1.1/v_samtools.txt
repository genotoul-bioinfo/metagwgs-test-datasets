samtools 1.15.1
Using htslib 1.16
Copyright (C) 2022 Genome Research Ltd.

Samtools compilation details:
    Features:       build=configure curses=yes 
    CC:             /opt/conda/conda-bld/samtools_1663435411790/_build_env/bin/x86_64-conda-linux-gnu-cc
    CPPFLAGS:       -DNDEBUG -D_FORTIFY_SOURCE=2 -O2 -isystem /opt/conda/envs/metagWGS/include
    CFLAGS:         -Wall -march=nocona -mtune=haswell -ftree-vectorize -fPIC -fstack-protector-strong -fno-plt -O2 -ffunction-sections -pipe -isystem /opt/conda/envs/metagWGS/include -fdebug-prefix-map=/opt/conda/conda-bld/samtools_1663435411790/work=/usr/local/src/conda/samtools-1.15.1 -fdebug-prefix-map=/opt/conda/envs/metagWGS=/usr/local/src/conda-prefix
    LDFLAGS:        -Wl,-O2 -Wl,--sort-common -Wl,--as-needed -Wl,-z,relro -Wl,-z,now -Wl,--disable-new-dtags -Wl,--gc-sections -Wl,--allow-shlib-undefined -Wl,-rpath,/opt/conda/envs/metagWGS/lib -Wl,-rpath-link,/opt/conda/envs/metagWGS/lib -L/opt/conda/envs/metagWGS/lib
    HTSDIR:         
    LIBS:           
    CURSES_LIB:     -ltinfow -lncursesw

HTSlib compilation details:
    Features:       build=configure libcurl=yes S3=yes GCS=yes libdeflate=yes lzma=yes bzip2=yes plugins=yes plugin-path=/opt/conda/envs/metagWGS/libexec/htslib htscodecs=1.3.0
    CC:             /opt/conda/conda-bld/htslib_1663255585694/_build_env/bin/x86_64-conda-linux-gnu-cc
    CPPFLAGS:       -DNDEBUG -D_FORTIFY_SOURCE=2 -O2 -isystem /opt/conda/envs/metagWGS/include
    CFLAGS:         -Wall -march=nocona -mtune=haswell -ftree-vectorize -fPIC -fstack-protector-strong -fno-plt -O2 -ffunction-sections -pipe -isystem /opt/conda/envs/metagWGS/include -fdebug-prefix-map=/opt/conda/conda-bld/htslib_1663255585694/work=/usr/local/src/conda/htslib-1.16 -fdebug-prefix-map=/opt/conda/envs/metagWGS=/usr/local/src/conda-prefix -fvisibility=hidden
    LDFLAGS:        -Wl,-O2 -Wl,--sort-common -Wl,--as-needed -Wl,-z,relro -Wl,-z,now -Wl,--disable-new-dtags -Wl,--gc-sections -Wl,--allow-shlib-undefined -Wl,-rpath,/opt/conda/envs/metagWGS/lib -Wl,-rpath-link,/opt/conda/envs/metagWGS/lib -L/opt/conda/envs/metagWGS/lib -fvisibility=hidden -rdynamic

HTSlib URL scheme handlers present:
    built-in:	 preload, data, file
    S3 Multipart Upload:	 s3w, s3w+https, s3w+http
    Amazon S3:	 s3+https, s3+http, s3
    libcurl:	 imaps, pop3, gophers, http, smb, gopher, sftp, ftps, imap, smtp, smtps, rtsp, scp, ftp, telnet, mqtt, https, smbs, tftp, pop3s, dict
    Google Cloud Storage:	 gs+http, gs+https, gs
    crypt4gh-needed:	 crypt4gh
    mem:	 mem
