# Test datasets repository

## samples with 3 genomes  

Use to test the binning step of metagWGS  BUT /!\ Chechkm2 is not repeatable, the functionnal test will not passed  /!\

### Input
3 samples (paired reads):
* 3_genomes_1.1 
* 3_genome_1.2  (clone of 3_genomes_1.1)
* 3_genomes_2

1 assembly per sample: 
* 3_genomes_1_assembly.fna
* 3_genomes_2_assembly.fna

### Script 
The script launcher_3genomes was used to obtain the output results. It launches metagwgs in different conditions (on the genologin server) with the inputs of 3genomes and compares the obtained results with the expected results.